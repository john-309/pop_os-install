# Pop_OS Install

The following are terminal commands for installing the applications and tools that I use on my Pop!_OS workstation.

## Install Applications

- `sudo apt install apt-utils htop vim git gimp inkscape golang python3 python3-dev python-is-python3 cowsay neofetch godot3 default-jdk deja-dup zsh steam keepassxc build-essential cmake gnome-tweaks menulibre nodejs npm filezilla remmina steam` 


## Add Flathub Repo

- `flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo`


## Install Flatpak Apps

- `flatpak install flathub com.discordapp.Discord com.github.jeromerobert.pdfarranger com.github.tchx84.Flatseal com.mojang.Minecraft com.obsproject.Studio com.spotify.Client org.telegram.desktop org.ferdium.Ferdium io.dbeaver.DBeaverCommunity com.github.Murmele.Gittyup fr.handbrake.ghb org.gnome.Extensions com.getmailspring.Mailspring com.transmissionbt.Transmission`


## Add & Install [Codium](https://vscodium.com/)

```
sudo rpmkeys --import https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg 
printf "[gitlab.com_paulcarroty_vscodium_repo]\nname=download.vscodium.com\nbaseurl=https://download.vscodium.com/rpms/\nenabled=1\ngpgcheck=1\nrepo_gpgcheck=1\ngpgkey=https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg\nmetadata_expire=1h" | sudo tee -a /etc/yum.repos.d/vscodium.repo 
sudo dnf install codium 
```


## Virtual Machines

- `sudo apt -y install bridge-utils cpu-checker libvirt-clients libvirt-daemon-system qemu qemu-kvm virt-manager virt-viewer libguestfs-tools python3-guestfs spice-client-gtk`




